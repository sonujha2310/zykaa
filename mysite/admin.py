from django.contrib import admin
from mysite.models import customer,city,Restaurant,category,food_item,cart,order,delivery_partner,order_details,feedback

admin.site.site_header = "Zyka|Fast Food Delivery"
admin.site.register(customer)
admin.site.register(city)
admin.site.register(Restaurant)
admin.site.register(category)
admin.site.register(food_item)
admin.site.register(cart)
admin.site.register(order)
admin.site.register(delivery_partner)
admin.site.register(order_details)
admin.site.register(feedback)